// TMW2 scripts.
// Authors:
//    4144
//    Qwerty Dragon
//    Vasily_Makarov
//    Jesusalva
// Description:
//    Allows to change language and talks about what happened to him.
//    Modified by Jesusalva for TMW2. She is the nurse and also does other minor tasks.
// Variables:
//    0 ShipQuests_Julia
// Values:
//    Julia:
//    10   Default, no quest given.
//    01   Need to see Julia.
//    02   Has been registered by Julia.

002-3,38,24,0	script	Juliet	NPC_JULIA,2,10,{

    function ynMenu {
        if (select(l("Yes, I do."), l("No, none.")) == 1) {
            return;
        }
        closedialog;
    }

    function sellFood {
        openshop;
        close;
        return;
    }



    function basicSkill {
        mes "";
        mesn;
        mesq l("Let me check into it...");
        next;
        adddefaultskills;
        mesq l("Here you go, everything is fixed.");
        emotion E_HAPPY;
        next;
        mesq l("Do you have any other questions for me?");
        next;
        ynMenu;
        return;
    }

    function chooseLang {
        mes "";
        mesn;
        mesq l("Of course! But beware that [@@https://www.transifex.com/akaras/saulc-tmw-fork|Translators@@] are always in demand!");
        next;
        mesq l("Tell me which language you speak and I will change the note on the ship passenger list.");
        next;

        asklanguage(LANG_IN_SHIP);

        mes "";
        mesn;
        mesq l("Ok, done.");

        if (getq(ShipQuests_Julia) == 2)
        {
            next;
            mesq l("Do you have any other questions for me?");
            next;
            ynMenu;
            return;
        }

        next;
        mesq l("I'm sure that you've got some questions for me, feel free to ask them, but first I need to tell you the rules of proper social conduct on board.");

        mesq l("Here they are.");
        next;

        narrator S_LAST_NEXT,
            l("There is a paper with some rules written on it.");

        GameRules 8 | 4;

        mesn;
        mesq l("Oh, and I almost forgot! Do not give the password of your room to anybody! I am the only one who has the other key and I won't ask for yours so keep it secret and try not to use the same password for any other room in the future.");
        next;
        mesq l("If you want to read this page again, there is a copy up on the left wall.");
        next;
        mesq l("You can also read The Book of Laws at any time to see the rules.");
        next;
        mesq l("I think I'm done with that now. Do you have any questions?");
        next;

        setq ShipQuests_Julia, 2;
        return;
    }

    function whereAmI {
        mes "";
        mesn;
        mesq l("You're on a ship, we're on our way to the oldest human city, Tulishmar.");
        next;
        mesq l("We should be there in a few days. For now, you can relax on the ship, or visit the island we're docked at! Its a small island, but a good place to get some exercise and stretch your legs.");
        next;
        mesq l("Do you have any other questions for me?");
        next;
        ynMenu;
        return;
    }

    function whatHappened {
        mes "";
        mesn;
        mesq l("We thought that you could help us understand this, all we know is that we found you cast in the sea, adrift on your raft.");
        next;
        mesq lg("You were in bad shape, you should be happy we found you before the sea killed you.");
        next;

        //select
        //    l("Sorry, but I can't tell you anything about that."),
        //    l("Nothing, sorry.");

        mes "";
        mesn;
        mesq l("Did you have any other questions for me?");
        next;
        ynMenu;
        return;
    }

    function readRules {
        mes "";
        mesn;
        mesq l("Of course, they are on the left wall, go have a look at them.");
        next;
        mesq l("Do you have any other questions for me?");
        next;
        ynMenu;
        return;
    }

    function mainMenu {
        do
        {
            .@q3 = getq(ShipQuests_Nard);
            .@q4 = getq(General_Narrator);

            selectd
                l("I am hungry. Can I buy some food here?"),
                rif(getskilllv(NV_BASIC) < 6, l("Something is wrong with me, I can't smile nor sit.")),
                lg("I made a mistake, I would like to change my language."),
                l("Could you explain to me where I am?"),
                l("What happened to me?"),
                l("Can I read these rules again?"),
                l("Nothing, sorry.");

            switch (@menu)
            {
                case 1: sellFood; break;
                case 2: basicSkill; break;
                case 3: chooseLang .@s$; break;
                case 4: whereAmI; break;
                case 5: whatHappened; break;
                case 6: readRules; break;
                case 7: closedialog; end;
            }
        } while (1);
    }

    mesn;
    mesq lg("Hello dear!");
    next;
    mesq l("What do you want today?");
    next;

    mainMenu;

OnTouch:
    .@q = getq(ShipQuests_Julia);
    if (.@q > 1) end;

    checkclientversion;

    mesn;
    mesq l("Hi, nice to see you!");
    next;
    mesq l("My name is Juliet, it is me who took care of you after we found you in the sea.");
    next;
    mesq lg("I'm glad to see you're okay.");
    next;
    if (getq(ShipQuests_Julia) < 2)
    {
        mesq l("I'm sure that you've got some questions for me, feel free to ask them, but first I need to tell you the rules of proper social conduct on board.");

        mesq l("Here they are.");
        next;

        narrator S_LAST_NEXT,
            l("There is a paper with some rules written on it.");

        GameRules 8 | 4;

        mesn;
        mesq l("Oh, and I almost forgot! Do not give the password of your room to anybody! I am the only one who has the other key and I won't ask for yours so keep it secret and try not to use the same password for any other room in the future.");
        next;
        mesq l("If you want to read this page again, there is a copy up on the left wall.");
        next;
        mesq l("You can also read The Book of Laws at any time to see the rules.");
        next;
        mesq l("I think I'm done with that now. Do you have any questions?");
        next;

        setq ShipQuests_Julia, 2;
    }
    //mesq lg("Could I ask you what your native language is? A sailor told me you're Russian, but another one told me you're French... I'm a bit lost. I will register you on the ship passenger list just after that.");
    //next;
    //chooseLang;
    mainMenu;
    end;

OnInit:
    .sex = G_FEMALE;
    .distance = 10;
	sellitem Cheese;
	sellitem Aquada;
	sellitem Piberries;

}
