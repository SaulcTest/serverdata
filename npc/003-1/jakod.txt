// TMW2 script.
// Author:
//    Saulc
//    Jesusalva
// Description:
//    Jakod replaces Luca. As there are no focus skills, Jakod teaches player about
//    magic and is their tutor on the matters of the Mana Stone.

003-1,109,129,0	script	Jakod	NPC_PLAYER,{


    mesn;
    if (BaseLevel < 30) mesq l("Hello there. I am looking for strong people, but you are not strong enough.");
    if (BaseLevel < 30) close;

    mes l("\"Ah, hello there, @@. You've grown quite skilled lately.", strcharinfo(0));
    if (MAGIC_LVL) mes l("I also see you have a level @@ magic skill!\"", MAGIC_LVL);
    if (!MAGIC_LVL) mes l("But you lack magic! That's a pity.\"");

    menu
        rif(!MAGIC_LVL, l("How do I get magic?")), L_HowTo,
        rif(MAGIC_LVL >= 1 && !getskilllv(NV_FIRSTAID), l("Can you teach me a basic tier 1 magic skill?")), L_Tier1,
        //rif(MAGIC_LVL >= 2 && !getskilllv(EVOL_MASS_PROVOKE),
        //rif(MAGIC_LVL >= 3 && !getskilllv(AL_TELEPORT),
        l("Good bye."), -;

    close;

L_HowTo:
    mes "";
    mesn;
    mesq l("Well, the first thing would be to get access to a Mana Stone. Halinarzo had the biggest mana stone mines, but it's now depleted.");
    next;
    mesn;
    mes l("\"That's the only hard part. Have at least level 40, do not neglect Intelligence nor Job level...");
    mes l("...And, if you're lucky, you can touch it and receive magic power.\"");
    next;
    mesn;
    mesq l("Having magic power is useless by itself, so you must visit the Magic Council and sign up in one of two classes.");
    next;
    mesn;
    mesq l("Your class selection is ##BPermanent##b and cannot be changed later. Do their tasks, and you'll be able to harness magic!");
    next;
    mesn;
    mesq l("The more levels and intelligence you have, more likely the Mana Stone will grant you more Magic Power. But that means nothing.");
    next;
    mesn;
    mesq l("Because in the end, you are in the hands of your class leader!"); // Rare: Some loner NPCs may grant you EXTRA skills beyond that.
    close;

L_Tier1:
    mesn;
    mesq l("Yeah, of course. I will teach you how to give first aid to people.");
    next;
    mesn;
    mesq l("It takes a lot of mana, and is not very powerful, but it is a must have for everyone.");
    next;
    mesn;
    mesq l("Ah... I am too lazy to ask for anything in exchange. Here. Take it. Free.");
    skill(NV_FIRSTAID,1,0);
    close;

OnInit:
    .@npcId = getnpcid(0, .name$);
    setunitdata(.@npcId, UDT_HEADTOP, NPCEyes);
    setunitdata(.@npcId, UDT_HEADMIDDLE, ForestArmor);
    setunitdata(.@npcId, UDT_HEADBOTTOM, JeansShorts);
    setunitdata(.@npcId, UDT_WEAPON, DeepBlackBoots);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 14);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 13);
    //setunitdata(.@npcId, UDT_HAIRSTYLE, 24);
    //setunitdata(.@npcId, UDT_HAIRCOLOR, 2);

    .sex = G_MALE;
    .distance = 3;
    end;
}
