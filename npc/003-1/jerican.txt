// TMW-2 Script
// Author:
//    Saulc
// Description:
//    An NPC in need of a quest

003-1,93,125,0	script	Jerican	NPC_PLAYER,{


hello;

OnInit:
    .@npcId = getnpcid(0, .name$);
    setunitdata(.@npcId, UDT_HEADTOP, NPCEyes);
    setunitdata(.@npcId, UDT_HEADMIDDLE, ForestArmor); // Maybe Bromenal Chest? ...Who is Jerican anyway?
    setunitdata(.@npcId, UDT_HEADBOTTOM, CottonShorts); // Maybe LeatherTrousers?
    setunitdata(.@npcId, UDT_WEAPON, DeepBlackBoots); // Boots
    setunitdata(.@npcId, UDT_HAIRSTYLE, 3);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 9);

    .sex = G_MALE;
    .distance = 5;
    end;
}
