// TMW2 scripts.
// Author:
//    Saulc
// Description:
//    Ched is from a quest.


003-1,62,48,0	script	Ched	NPC_PLAYER,{

    hello;


OnInit:
    .@npcId = getnpcid(0, .name$);
    setunitdata(.@npcId, UDT_HEADTOP, SamuraiHelmet);
    setunitdata(.@npcId, UDT_HEADMIDDLE, SilkRobe);
    setunitdata(.@npcId, UDT_HEADBOTTOM, NPCEyes);
    setunitdata(.@npcId, UDT_WEAPON, CandorBoots); // Boots
    setunitdata(.@npcId, UDT_HAIRSTYLE, 7);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 17);

    .sex = G_MALE;
    .distance = 5;
    end;
}
