// Author:
//    Saulc
// TODO: Could be repeatable quest (eg. 60 GP for a cake every day, so you can have a 10 GP profit selling cakes)

003-1,90,144,0	script	Sarah	NPC_FEMALE,{

    .cake = CherryCake;
    .reward = SerfHat;
    
    function quest_completed{
        speech S_FIRST_BLANK_LINE | S_LAST_NEXT,
            l("It was so tasty, I can't eat anything more... Thank you.");
            close;
    }

    function quest_open {
        if (countitem(.cake) > 5)
        {
            speech S_FIRST_BLANK_LINE | S_LAST_NEXT,
                l("You brought me 6 @@ ! Here is your @@, as promised.",getitemlink(.cake), getitemlink(.reward));
            delitem .cake,6;
            getitem .reward,1;
            setq TulimsharQuest_Sarah, 1;
            close;
        }
        else 
        {
            speech S_FIRST_BLANK_LINE,
                l("Sorry, that is not the cake I love.");
            close;
        }
    }

    function quest_started {
        speech S_FIRST_BLANK_LINE | S_LAST_NEXT,
            l("Oh, Welcome then.");
        speech S_LAST_NEXT,
            l("Can you bring me 6 piece of Cherry Cake? Pretty please?");
        do
        {
        select
            l("Here is it!"),
            menuaction(l("Quit"));

        switch (@menu)
        {
            case 1:
                quest_open;
                break;
        }
    } while (@menu != 2);
    }
   
    do
    {
        .@chest = getq(TulimsharQuest_Sarah);
        if (.@chest == 1)
            goto quest_completed;
        select
            rif(.@chest == 0,
            l("Hello, I'm new here! Can I help you?")),
            menuaction(l("Quit"));

        switch (@menu)
        {
            case 1:
                quest_started;
                break;
        }
    } while (@menu != 2);

    closedialog;
    goodbye;
    close;

OnInit:
    .@npcId = getnpcid(0, .name$);
    setunitdata(.@npcId, UDT_HEADTOP, TerraniteArmor);
    setunitdata(.@npcId, UDT_HEADMIDDLE, RaidTrousers);
    setunitdata(.@npcId, UDT_HEADBOTTOM, NPCEyes);
    setunitdata(.@npcId, UDT_WEAPON, CandorBoots); // Boots
    setunitdata(.@npcId, UDT_HAIRSTYLE, 19);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 16);

    .sex = G_FEMALE;
    .distance = 5;
    end;
}

