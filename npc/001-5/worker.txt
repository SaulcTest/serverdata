// TMW2 Script
// Author: Jesusalva
// Workers from Worker Day

001-5,23,73,0	script	Soren	NPC_CONSTR_WORKER,{
    mesn;
    mesq l("You can get @@ anywhere, although here is a little easier to get.", getitemlink(Pearl));
    next;
    mesn;
    mesq l("You can trade them for quite nice items with my friend over there.");
    close;
}

001-5,75,69,0	script	Simon	NPC_CONSTR_WORKER,{
    mesn;
    mesq l("Hey dude. During this event you can trade one @@ for more... useful items.", getitemlink(Pearl));
    if (countitem(Pearl) == 0)
        close;
    next;
    select(
        l("12x Strange Coins"),
        l("2x Snake Egg"),
        l("2x Bronze Gift"),
        l("1600 GP"),
        rif(countitem(Pearl) >= 2, l("Trade 2 Pearl for a Silver Gift + a Bronze Gift")),
        l("Nothing right now.")
    );
    switch (@menu) {
        case 1:
            delitem Pearl, 1;
            getitem StrangeCoin, 12;
            break;
        case 2:
            delitem Pearl, 1;
            getitem SnakeEgg, 2;
            break;
        case 3:
            delitem Pearl, 1;
            getitem BronzeGift, 2;
            break;
        case 4:
            delitem Pearl, 1;
            Zeny=Zeny+1600;
            break;
        case 5:
            delitem Pearl, 2;
            getitem SilverGift, 1;
            getitem BronzeGift, 1;
            break;

    }

    close;


}


