// Evol scripts.
// Authors:
//    4144
//    Qwerty Dragon
//    Reid
//    Vasily_Makarov
//    Jesusalva
// Description:
//    Starting script of Evol Online, modified for TMW2.

000-0,23,20,0	script	Sailors	NPC_SAILORS,6,6,{

OnTouch:
    //checkclientversion;
    .@lang = requestlang();
    if (.@lang >= 0 && .@lang <= 9) Lang = .@lang;

    showavatar 3;

    mesn "Narrator";
    mes col(l("You open your eyes. The salt water is not helping your vision."), 9);
    next;
    mes col(l("A blur shape forms in your front. It seems to be a large ship."), 9);
    next;
    mes col(l("Wait. A ship? Where are you, after all?"), 9);
    next;
    mes col(l("Your body aches. You can't remember what happened."), 9);
    next;
    mes col(l("But this is not important now. The ships sailors are shouting at you!"), 9);
    next;

    showavatar NPC_ORC_MAN;
    setcamnpc "Sailors", -64, -32;
    mesn "Orc Voice";
    mesq lg("Hey kid! Can you hear me?");
    next;

    showavatar NPC_SILVIO;
    setcamnpc "Sailors", 0, -32;
    mesn "Human Voice";
    mesq lg("Hear you? How do you even know she can understand you?!", "Hear you? How do you even know he can understand you?!");
    next;

    showavatar NPC_TRITAN_MAN_SAILOR;
    setcamnpc "Sailors", 144, -80;
    mesn "Tritan Voice";
    mesq lg("We speak various languages on this world. Let's try gesturing!");
    next;

    showavatar NPC_RAIJIN_MAN;
    setcamnpc;
    mesn "Raijin Voice";
    mesq l("Hey you! Can you hear us? Are you okay?");
    next;
    restorecam;

    asklanguage(LANG_ON_SEA);

    showavatar NPC_SAILORS;
    setcamnpc;
    mes "";
    mesn;
    mesq lg("Oh, she's still alive!", "Oh, he's still alive!");
    next;
    showavatar NPC_TRITAN_MAN_SAILOR;
    setcamnpc "Sailors", 144, -80;
    mesq lg("This girl needs help, we need to rescue her!", "This guy needs help, we need to rescue him!");
    next;
    restorecam;

    showavatar;
    mesn "Narrator";
    mes col(l("The sailors take you aboard their ship."), 9);
    next;
    mes col(l("You faint from the pain. At least now, you are in good hands."), 9);
    next;
    mes col(l("Click on the NPCs (Non-Player Characters) around you to continue the introduction."), 9);
    next;

    warp "000-0-0", 26, 28;

    closedialog;
    close;

OnInit:
    .sex = G_MALE;
    end;
}
