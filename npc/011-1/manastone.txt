// TMW2 Script
// Author:
//  Jesusalva
// Description:
//  The last Mana Stone in the whole world, which is not owned by the Magic Council
//  or the Monster King.
//  It actually hates (or rather, fears) everybody, and is not always willing to
//  talk with people. May hide itself within the walls during these occasions.
//  It may not hate some very specific things which are lore-related.
//
//  Notes: During sieges, Monster King and Human Council apparitions, it may hide
//  itself.

011-1,0,0,0	script	Mana Stone	NPC_MANA_STONE,{

    if (BaseLevel < 40) goto L_NotWorthy;
    mesn;
    mes l("The mighty Mana Stone does not reacts against you.");
    mes l("Although this particular one seems to hate everyone and everything, it recognizes your strength.");
    mes l("If you fell ready, perhaps you should touch it?");
    mes "";
    menu
        l("Touch it!"), L_Level,
        l("Take it!"), L_NotWorthy2,
        l("Break it!"), L_NotWorthy2,
        l("Leave it alone!"), -;
    close;


L_NotWorthy:
    percentheal -70+BaseLevel, -100+BaseLevel;
    npctalk3 l("You are not worthy!");
    end;

L_Level:
    if (MAGIC_LVL == 0 && readparam(bInt) >= 30 && BaseLevel >= 40 && JobLevel >= 10 && readparam(Sp) == readparam(MaxSp)) goto L_LevelUp;

    // Everything below this line is garbage
    if (MAGIC_LVL == 1 && readparam(bInt) >= 60 && BaseLevel >= 60 && JobLevel >= 30 && readparam(Sp) == readparam(MaxSp)) goto L_LevelUp;
    if (MAGIC_LVL == 2 && readparam(bInt) >= 90 && BaseLevel >= 80 && JobLevel >= 50 && readparam(Sp) == readparam(MaxSp)) goto L_LevelUp;
    if (MAGIC_LVL == 3 && readparam(bInt) >= 120 && BaseLevel >= 100 && JobLevel >= 70 && readparam(Sp) == readparam(MaxSp)) goto L_LevelUp;
    if (MAGIC_LVL == 4 && readparam(bInt) >= 150 && BaseLevel >= 120 && JobLevel >= 90 && readparam(Sp) == readparam(MaxSp)) goto L_LevelUp;
    if (MAGIC_LVL == 5 && readparam(bInt) >= 180 && BaseLevel >= 140 && JobLevel >= 110 && readparam(Sp) == readparam(MaxSp)) goto L_LevelUp;
    if (MAGIC_LVL == 6 && readparam(bInt) >= 210 && BaseLevel >= 160 && JobLevel >= 120 && readparam(Sp) == readparam(MaxSp)) goto L_LevelUp;
    if (MAGIC_LVL >= 7) npctalk3 l("You already got all power I could grant you!");
    if (is_gm()) percentheal -20, -50;
    if (MAGIC_LVL >= 7 || is_gm()) close;

L_NotWorthy2:
    if (is_gm()) movenpc(.name$, rand(200), rand(200));
    if (is_gm()) close;
    percentheal -20, -50;
    npctalk3 l("You are not worthy!");
    end;

L_LevelUp:
    mes "";
    mes l("A great rush of mana flows though you.");
    if (!MAGIC_LVL) mes l("Magic Power is granted to you, but you die from it.");
    if (MAGIC_LVL) mes l("More Magic Power is granted to you, but you die from it.");
    MAGIC_LVL = MAGIC_LVL+1;
    percentheal -100, -100;
    close;

OnInit:
    movenpc .name$, rand(200), rand(200);
    .sex = G_OTHER;
    .distance = 6;
    end;

OnClock0030:
OnClock0120:
OnClock0210:
OnClock0300:
OnClock0450:
OnClock0540:
OnClock0630:
OnClock0720:
OnClock0810:
OnClock0900:
OnClock1050:
OnClock1140:
OnClock1230:
OnClock1320:
OnClock1410:
OnClock1500:
OnClock1650:
OnClock1740:
OnClock1830:
OnClock1920:
OnClock2010:
OnClock2100:
OnClock2250:
OnClock2340:
    npctalk "Those who are worthy, may get my magic. For the others, death awaits!";
    movenpc .name$, rand(200), rand(200);
    end;

}
