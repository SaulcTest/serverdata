// TMW2 SCRIPT.
// Author:
//    The Mana World Brazil
function	script	mushroomWarp	{
    heal -5, -5;
    warp getmapname(), 0, 0;
    @n = rand(3);
    if (@n == 0) dispbottom l("Uhh... What happened...");
    if (@n == 1) dispbottom l("The world is spiniiiiiiiing...");
    if (@n == 2) dispbottom l("Ah... What is happening to meeeeeeee?");
    return;
}

001-4,252,184,0	script	#001-4_252x184	NPC_FANCY_CIRCLE,0,0,{
    mes l("A bright and mysterious mushroom!!!");
    mes l("Only getting closer to find out what kind this one is.");
    close;

OnTouch:
    callfunc "mushroomWarp";
    close;
}

001-4,259,128,0	script	#001-4_259x128	NPC_FANCY_CIRCLE,0,0,{
    mes l("A bright and mysterious mushroom!!!");
    mes l("Only getting closer to find out what kind this one is.");
    close;

OnTouch:
    callfunc "mushroomWarp";
    close;
}

001-4,252,255,0	script	#001-4_252x255	NPC_FANCY_CIRCLE,0,0,{
    mes l("A bright and mysterious mushroom!!!");
    mes l("Only getting closer to find out what kind this one is.");
    close;

OnTouch:
    callfunc "mushroomWarp";
    close;
}

001-4,180,270,0	script	#001-4_180x270	NPC_FANCY_CIRCLE,0,0,{
    mes l("A bright and mysterious mushroom!!!");
    mes l("Only getting closer to find out what kind this one is.");
    close;

OnTouch:
    callfunc "mushroomWarp";
    close;
}

001-4,124,267,0	script	#001-4_124x267	NPC_FANCY_CIRCLE,0,0,{
    mes l("A bright and mysterious mushroom!!!");
    mes l("Only getting closer to find out what kind this one is.");
    close;

OnTouch:
    callfunc "mushroomWarp";
    close;
}

001-4,44,243,0	script	#001-4_44x243	NPC_FANCY_CIRCLE,0,0,{
    mes l("A bright and mysterious mushroom!!!");
    mes l("Only getting closer to find out what kind this one is.");
    close;

OnTouch:
    callfunc "mushroomWarp";
    close;
}

001-4,41,180,0	script	#001-4_41x180	NPC_FANCY_CIRCLE,0,0,{
    mes l("A bright and mysterious mushroom!!!");
    mes l("Only getting closer to find out what kind this one is.");
    close;

OnTouch:
    callfunc "mushroomWarp";
    close;
}

001-4,58,127,0	script	#001-4_58x127	NPC_FANCY_CIRCLE,0,0,{
    mes l("A bright and mysterious mushroom!!!");
    mes l("Only getting closer to find out what kind this one is.");
    close;

OnTouch:
    callfunc "mushroomWarp";
    close;
}

001-4,56,51,0	script	#001-4_56x51	NPC_FANCY_CIRCLE,0,0,{
    mes l("A bright and mysterious mushroom!!!");
    mes l("Only getting closer to find out what kind this one is.");
    close;

OnTouch:
    callfunc "mushroomWarp";
    close;
}

001-4,114,39,0	script	#001-4_114x39	NPC_FANCY_CIRCLE,0,0,{
    mes l("A bright and mysterious mushroom!!!");
    mes l("Only getting closer to find out what kind this one is.");
    close;

OnTouch:
    callfunc "mushroomWarp";
    close;
}

001-4,183,37,0	script	#001-4_183x37	NPC_FANCY_CIRCLE,0,0,{
    mes l("A bright and mysterious mushroom!!!");
    mes l("Only getting closer to find out what kind this one is.");
    close;

OnTouch:
    callfunc "mushroomWarp";
    close;
}

001-4,244,57,0	script	#001-4_244x57	NPC_FANCY_CIRCLE,0,0,{
    mes l("A bright and mysterious mushroom!!!");
    mes l("Only getting closer to find out what kind this one is.");
    close;

OnTouch:
    callfunc "mushroomWarp";
    close;
}

001-4,154,136,0	script	#001-4_154x136	NPC_FANCY_CIRCLE,0,0,{
    mes l("A bright and mysterious mushroom!!!");
    mes l("Only getting closer to find out what kind this one is.");
    close;

OnTouch:
    callfunc "mushroomWarp";
    close;
}

001-4,142,173,0	script	#001-4_142x173	NPC_FANCY_CIRCLE,0,0,{
    mes l("A bright and mysterious mushroom!!!");
    mes l("Only getting closer to find out what kind this one is.");
    close;

OnTouch:
    callfunc "mushroomWarp";
    close;
}

