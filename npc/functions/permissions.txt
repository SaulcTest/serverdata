// TMW2 scripts.
// Author:
//    Jesusalva
// Description:
//    Checks player GM levels

function	script	is_gm	{
    return (getgmlevel() >= 80);
}

function	script	is_admin	{
    return (getgmlevel() >= 99);
}

function	script	is_staff	{
    return (getgmlevel() >= 5);
}
